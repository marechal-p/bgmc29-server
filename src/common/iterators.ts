export namespace Iterator {

    export function *Enumerate<T>(sequence: Iterable<T>): IterableIterator<{ index: number, value: T }> {
        let i = 0
        for (const value of sequence) {
            yield { index: i++, value }
        }
    }

    export function AtIndex<T>(sequence: Iterable<T>, index: number = 0): T | undefined {
        if (index < 0) return undefined
        const iterator = sequence[Symbol.iterator]()
        let i = 0
        while (true) {
            const next = iterator.next()
            if (next.done) break
            if (i++ >= index) return next.value
        }
        return undefined
    }

}
