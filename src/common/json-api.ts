export interface JsonResponse {
    success: boolean
    [key: string]: any
}
export namespace JsonResponse {
    export class Success implements JsonResponse {
        success = true
        constructor(properties: object) {
            Object.assign(this, properties)
        }
    }
    export class Failure implements JsonResponse {
        success = false
        constructor(
            public readonly message: string | null,
        ) { }
    }
}
