import { PlayerState } from './player-data'

export interface GameWebMessage<T = any> {
    type: string
    body: T
}

export interface GameServerEvents {
    'player-list-update': [PlayerState[]]
}
