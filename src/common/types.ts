export interface Constructor<A extends any[] = any[], I extends any = any> {
    new(...args: A): I
}
export namespace Constructor {
    export type Arguments<T extends Constructor> = T extends Constructor<infer A, any> ? A : never
    export type Instance<T extends Constructor> = T extends Constructor<any, infer I> ? I : never
}

export type MaybePromise<T> = T | Promise<T>
