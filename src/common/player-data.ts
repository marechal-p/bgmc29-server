export interface PlayerState {
    name: string
    health: number
    kills: number
}
