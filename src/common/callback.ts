export type Callback = (...args: any[]) => any
export namespace Callback {

    export type Arguments<T extends Callback> = T extends (...args: infer A) => any ? A : never
    export type Return<T extends Callback> = T extends (...args: any[]) => infer R ? R : never

    export class List<T extends Callback> {

        protected _callbacks: T[] = []

        public push(...callbacks: T[]): this {
            this._callbacks.push(...callbacks)
            return this
        }

        public remove(...callbacks: T[]): this {
            for (const callback of callbacks) {
                const index = this._callbacks.indexOf(callback)
                if (index !== -1) this._callbacks.splice(index, 1)
            }
            return this
        }

        public apply(context: any, ...args: Arguments<T>): void {
            for (const callback of this._callbacks) {
                callback.apply(context, args)
            }
        }

        public call(...args: Arguments<T>): void {
            this.apply(undefined, ...args)
        }

    }

}
