export interface Disposable {
    dispose(): void
}
export function Disposable(action: () => void): Disposable {
    return {
        dispose: action,
    }
}
export namespace Disposable {

    export function Is(object: any): object is Disposable {
        return object && typeof object.dispose === 'function'
    }

    export function Null(): Disposable {
        return Disposable(() => undefined)
    }

    export class Collection implements Disposable {

        protected _disposed = false
        protected readonly _disposables: Disposable[] = []

        public get disposed() {
            return this._disposed
        }

        public push(...disposables: Array<Disposable | (() => void)>): this {
            this._disposables.push(...disposables.map(disposable =>
                typeof disposable === 'object' ? disposable : Disposable(disposable)))
            return this
        }

        public dispose() {
            if (!this._disposed) {
                for (const disposable of this._disposables) {
                    disposable.dispose()
                }
                this._disposables.length = 0
                this._disposed = true
            }
        }

    }

}
