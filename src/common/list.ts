export interface List<T> extends Iterable<T> {

    length: number

    extend_tail(...lists: Iterable<T>[]): this
    push_tail(...items: T[]): this
    pop_tail(): T

    extend_head(...lists: Iterable<T>[]): this
    push_head(...items: T[]): this
    pop_head(): T

    splice(index: number, delete_count: number, ...items: T[]): this
    insert(index: number, ...items: T[]): this
    copy(start?: number, end?: number): this

    index_of(item: T): IterableIterator<number>
    find_item(predicate: (item: T, index: number) => boolean): IterableIterator<T>
    find_index(predicate: (item: T, index: number) => boolean): IterableIterator<number>

    remove(...items: T[]): this
    clear(): this
}

export class ArrayList<T> implements List<T> {

    protected readonly _elements: T[]

    constructor(
        elements: Iterable<T> = [],
    ) {
        this._elements = Array.from(elements)
    }

    public [Symbol.iterator](): IterableIterator<T> {
        return this._elements[Symbol.iterator]()
    }

    public get length(): number {
        return this._elements.length
    }

    public extend_tail(...lists: Iterable<T>[]): this {
        for (const list of lists) {
            this.push_tail(...list)
        }
        return this
    }

    public push_tail(...items: T[]): this {
        this._elements.push(...items)
        return this
    }

    public pop_tail(): T {
        return this._elements.pop()
    }

    public extend_head(...lists: Iterable<T>[]): this {
        for (const list of lists) {
            this.push_head(...list)
        }
        return this
    }

    public push_head(...items: T[]): this {
        this._elements.unshift(...items)
        return this
    }

    public pop_head(): T {
        return this._elements.shift()
    }

    public splice(start: number, delete_count: number, ...items: T[]): this {
        this._elements.splice(start, delete_count, ...items)
        return this
    }

    public insert(index: number, ...items: T[]): this {
        this._elements.splice(index, 0, ...items)
        return this
    }

    public copy(start?: number, end?: number): this {
        return new this.constructor.prototype(this._elements.slice(start, end))
    }

    public *index_of(item: T): IterableIterator<number> {
        let index: number
        while (true) {
            index = this._elements.indexOf(item, index)
            if (index === -1) break
            yield index
        }
    }

    public *find_item(predicate: (item: T, index: number) => boolean): IterableIterator<T> {
        let index = 0
        for (const item of this) {
            if (predicate(item, index++)) yield item
        }
    }

    public *find_index(predicate: (item: T, index: number) => boolean): IterableIterator<number> {
        let index = 0
        for (const item of this) {
            if (predicate(item, index++)) yield index
        }
    }

    public remove(...items: T[]): this {
        for (const item of items) {
            while (true) {
                const index = this._elements.indexOf(item)
                if (index === -1) break
                this._elements.splice(index, 1)
            }
        }
        return this
    }

    public clear(): this {
        this._elements.length = 0
        return this
    }

}
export const List = ArrayList

// export class LinkedList<T> implements List<LinkedList.Node<T>> {

//     protected head?: LinkedList.Node<T>
//     protected tail?: LinkedList.Node<T>

//     constructor(
//         elements: Iterable<T>,
//     ) {
//         let i = 0
//         let previous: LinkedList.Node<T>
//         for (const element of elements) {
//             const current = new LinkedList.Node(element, { previous })
//             previous.next = current
//             if (i++ === 0) {
//                 this.head = current
//             }
//             this.tail = current
//         }
//     }

//     public get length(): number {
//         let length = 0
//         let node = this.head
//         while (node) {
//             node = node.next
//             length++
//         }
//         return length
//     }

//     public extend_tail(...lists: Iterable<LinkedList.Node<T>>[]): this {

//         return this
//     }

// }
// export namespace LinkedList {

//     export class Node<T> {

//         public previous: Node<T> | undefined
//         public next: Node<T> | undefined

//         constructor(
//             public value: T,
//             options: {
//                 previous?: Node<T>,
//                 next?: Node<T>,
//             } = { },
//         ) {
//             this.previous = options.previous
//             this.next = options.next
//         }

//     }

// }
