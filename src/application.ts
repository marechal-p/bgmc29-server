import { resolve } from 'path'

export namespace Application {

    export const Root = resolve(__dirname, '..')

    export function Resolve(...paths: string[]): string {
        return resolve(Application.Root, ...paths)
    }

    export const MAX_REGISTRATION_PER_HOUR = 5

}
