import { Socket } from 'net'
import express from 'express'
import WebSocket from 'ws'
import http from 'http'

import { LogicManager } from './game/singletons/logic-manager'
import { MatchServer } from './game/match/match-server'

import { WebServer } from './web/web-server'

/**
 * A GameServer represent the main server instance,
 * managing each match server as well as the web server.
 */
export class GameServer {

    public readonly http_request_handler: express.Application

    public readonly matches = new Map<number, MatchServer>()
    protected match_counter = 0

    protected readonly websocket_server_routes = new Map<string, WebSocket.Server>()
    protected readonly http_server: http.Server

    protected server_loop: NodeJS.Timer
    protected delta_time: number // seconds

    protected readonly web_server: WebServer

    /**
     * @param frequency Update frequency (Hz)
     */
    constructor(
        public readonly host: string,
        public readonly port: number,
        protected frequency: number = 10,
    ) {
        this.delta_time = 1 / frequency

        this.http_request_handler = express()
        this.http_server = http.createServer(this.http_request_handler)
            .listen(port, host, () => console.log('HTTP Server is listening.'))
        this.http_server.on('upgrade', this.upgrade.bind(this))

        this.web_server = new WebServer(this)
        this.create_match_server()
        this.start()
    }

    public create_websocket_server(path: string, options?: WebSocket.ServerOptions): WebSocket.Server {
        const wss = new WebSocket.Server({ noServer: true, ...options })
        const endpoint = '/ws/' + path
        this.websocket_server_routes.set(endpoint, wss)
        console.debug(`Created WebSocket Server listening at ${endpoint}`)
        return wss
    }

    public create_match_server(): MatchServer {
        const match_server = new MatchServer(this, this.match_counter++)
        this.matches.set(match_server.id, match_server)
        console.debug(`Created MatchServer #${match_server.id}`)
        LogicManager.Instance.register(match_server)
        return match_server
    }

    protected start(): void {
        this.server_loop = setInterval(
            this.main_loop.bind(this),
            this.delta_time * 1000)
    }

    protected upgrade(request: http.IncomingMessage, socket: Socket, head: Buffer): void {
        const wss = this.websocket_server_routes.get(request.url)
        if (wss) wss.handleUpgrade(request, socket, head,
            websocket => wss.emit('connection', websocket, request))
        else {
            socket.write(Buffer.from([
                'HTTP/1.1 403 Forbidden',
                `Date: ${(new Date()).toUTCString()}`,
            ].join('\r\n') + '\r\n\r\n', 'utf8'))
            socket.end()
        }
    }

    protected main_loop(): void {
        LogicManager.Instance.update(this.delta_time)
    }

}
