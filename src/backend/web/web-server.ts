import * as WebSocket from 'ws'
import express from 'express'
import uuid4 from 'uuid/v4'

import { JsonResponse } from '../../common/json-api'
import { Application } from '../../application'
import { Database } from '../../database'

import { PlayerTable } from '../../database/player-table'
import { IpTable } from '../../database/ip-table'

import { GameServer } from '../game-server'
import { Logic } from '../game/logic'
import { Player } from '../player'

export class WebServer implements Logic {

    protected readonly webclient_websocket_server: WebSocket.Server

    constructor(
        protected readonly game_server: GameServer,
    ) {
        this.attach_routes()
    }

    public update() { }

    protected attach_routes(): void {
        const handler = this.game_server.http_request_handler

        // Global libraries:
        this.serve_library(handler, 'react.production.js', 'react/umd/react.production.min.js')
        this.serve_library(handler, 'react-dom.production.js', 'react-dom/umd/react-dom.production.min.js')

        handler

            // General resources:
            .get('/r/:what', (request, response) => this.serve_file(response, request.params.what, WebServerData.Dist))
            .get('/s/:what', (request, response) => this.serve_file(response, request.params.what, WebServerData.Static))
            .get('/', (request, response) => this.serve_file(response, 'index.html', WebServerData.Static))

            // Player registration endpoint:
            .get('/player/register', this.register_player.bind(this))

            // Match list endpoint:
            .get('/matches', this.list_matches.bind(this))
    }

    protected async register_player(request: express.Request, response: express.Response): Promise<void> {
        const { remoteAddress, remotePort } = request.connection
        const db = await Database.connect()
        try {

            // Rate limit:
            const results = await db.query(IpTable.GetMostRecentIpEntries(remoteAddress, Application.MAX_REGISTRATION_PER_HOUR))
            if (results.rows[0].count >= Application.MAX_REGISTRATION_PER_HOUR) {
                throw new Error(`Too many connections from ${remoteAddress}`)
            }
            await db.query(IpTable.InsertIpEntry(remoteAddress))

            // Extract username:
            const raw_username = request.headers['bgmc-username']
            const username: string | undefined = typeof raw_username === 'object' ?
                raw_username[0] : raw_username

            // Validate username:
            if (!Player.ValidateUsername(username)) {
                throw new Error(`invalid username: ${username}`)
            }

            // Create player entry:
            const uuid = uuid4()
            await db.query(PlayerTable.InsertPlayer(uuid, username))

            // Notify/finalize:
            console.debug(`${remoteAddress}:${remotePort} ${uuid} registered!`)
            response.send(new JsonResponse.Success({ uuid }))

        } catch (error) {
            console.error(error)
            response.status(403)
            response.send(new JsonResponse.Failure(
                error.constraint === PlayerTable.Constraints.UniquePlayerUsername ?
                    'Username is already in use.' : null))

        } finally {
            db.release()
        }
    }

    protected list_matches(request: express.Request, response: express.Response): void {
        response.send([...this.game_server.matches.keys()])
    }

    protected serve_file(response: express.Response, file: string, root: string): void {
        response.sendFile(file, { root }, error => { if (error) response.sendStatus(404) })
    }

    protected serve_library(app: express.Application, url: string, file: string): express.Application {
        return app.get(`/l/${url}`, (request, response) => response.sendFile(file, { root: WebServerData.NodeModules }))
    }

}
export namespace WebServerData {
    export const NodeModules = Application.Resolve('node_modules')
    export const Static = Application.Resolve('static')
    export const Dist = Application.Resolve('dist')
}
