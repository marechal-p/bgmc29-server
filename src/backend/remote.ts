import * as http from 'http'

export interface Remote {
    host: string
    port: number
}
export namespace Remote {

    export function FromRequest(request: http.IncomingMessage): Remote {
        return {
            host: request.connection.remoteAddress,
            port: request.connection.remotePort,
        }
    }

    export function Id(remote: Remote): string {
        return `${remote.host}:${remote.port}`
    }

}
