
export class Player {

    public readonly uuid: string
    public readonly username: string
    public readonly kills: number = 0

    public constructor(
        data: Player.Data,
    ) {
        this.uuid = data.uuid
        this.username = data.username
        this.kills = data.kills
    }

}
export namespace Player {

    export const USERNAME_LENGTH_MIN = 3
    export const USERNAME_LENGTH_MAX = 20

    export function ValidateUsername(username: string | undefined): username is string {
        if (
            username &&
            username.length >= Player.USERNAME_LENGTH_MIN &&
            username.length <= Player.USERNAME_LENGTH_MAX &&
            /\w|[-_.><]/.test(username)
        ) {
            return true
        }
        return false
    }

    export const KEY = Symbol('Player')

    export interface Data {
        uuid: string
        username: string
        kills: number
    }

}
