const ADMIN_KEY = process.env['ADMIN_KEY'] || 'admin'

export namespace Paths {
    export namespace Administration {
        export const Dashboard = `/${ADMIN_KEY}`
    }
}
