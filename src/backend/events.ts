export interface Event {
    type: string
}
export namespace Event {

    export class Factory<E extends object, A extends any[] = [E]> {

        constructor(
            public readonly type: string,
            protected readonly factory?: (...args: A) => E,
        ) { }

        public is(object: any): object is (Event & E) {
            return object.type === this.type
        }

        public create(...args: A): Event & E {
            const event = this.factory ?
                this.factory(...args) : args[0]
            return {
                ...event,
                type: this.type,
            }
        }

    }

    export interface Handler {

        /**
         * @param event The event to handle.
         * @returns Was the event handled?
         */
        handle(event: Event): boolean | void
    }

    export namespace Handler {

        export class Collection implements Handler {

            constructor(
                public readonly handlers: Handler[] = [],
            ) { }

            public handle(event: Event): boolean {
                let handled = false
                for (const handler of this.handlers) {
                    handled = handled || (handler.handle(event) === true)
                }
                return handled
            }

        }

    }

}
