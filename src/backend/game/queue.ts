export class Queue<T> {

    protected readonly elements: T[] = []

    public queue(...elements: T[]): this {
        this.elements.push(...elements)
        return this
    }

    public pop_left(): T | undefined {
        return this.elements.shift()
    }

    public pop_right(): T | undefined {
        return this.elements.splice(-1, 1)[0]
    }

    public *iter_pop_left(): Iterable<T> {
        let buffer: T
        while (buffer = this.pop_left()) {
            yield buffer
        }
    }

    public *iter_pop_right(): Iterable<T> {
        let buffer: T
        while (buffer = this.pop_right()) {
            yield buffer
        }
    }
}
