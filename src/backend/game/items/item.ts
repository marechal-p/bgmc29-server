export class Item {

    protected static ID = 0

    public readonly id = Item.ID++

    constructor(
        public readonly type: Item.Type,
        public readonly name: string,
    ) { }

}
export namespace Item {

    export class Type<T extends any[] = []> {

        constructor(
            public readonly type: number,
            public readonly name: string,
            public readonly create?: (...args: T) => Item,
        ) {
            if (!create) { this.create = () => new Item(this, name) }
        }

    }

}
