import { Item } from './item'

export class Weapon extends Item {

    public readonly category: 'weapon'

}
