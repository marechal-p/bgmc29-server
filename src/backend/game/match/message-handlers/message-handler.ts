import WebSocket from 'ws'

import { Event } from '../../../events'

export abstract class MessageHandler implements Event.Handler {

    constructor(
        public readonly server: WebSocket.Server,
    ) { }

    public handle(event: Event): boolean | void {
        const message = this.convert(event)
        if (message) this.broadcast(message)
    }

    protected broadcast(message: WebSocket.Data) {
        for (const client of this.server.clients) {
            client.send(message)
        }
    }

    protected abstract convert(event: Event): WebSocket.Data | void
}
