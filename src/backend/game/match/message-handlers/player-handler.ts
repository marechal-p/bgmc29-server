import { Event } from '../../../events'

import { MatchEvent } from '../match-events'
import { Message } from '../../protocol'

import { MessageHandler } from './message-handler'

export class PlayerHandler extends MessageHandler {

    protected convert(event: Event): Buffer {
        if (MatchEvent.EntityCreate.is(event)) {
            return Message.Encode.EntityCreate({
                entity_id: event.entity.id,
                entity_type: event.entity.type,
            })
        }
        else if (MatchEvent.EntityUpdate.is(event)) {
            return Message.Encode.EntityUpdate(event.entities)
        }
        else if (MatchEvent.EntityAttack.is(event)) {
            return Message.Encode.EntityAttack(event)
        }
        else if (MatchEvent.EntityDelete.is(event)) {
            return Message.Encode.EntityDelete(event.entity)
        }
    }

}
