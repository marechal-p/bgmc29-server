import { Disposable } from '../../../../common/disposable'
import { List } from '../../../../common/list'

import { EntityManager } from '../../match/managers/entity-manager'
import { LogicManager } from '../../singletons/logic-manager'
import { Entity } from '../../entities/entity'
import { GameEntities } from '../../entities'
import { Vector3 } from '../../vector'
import { Logic } from '../../logic'

export class ZombieManager implements Logic, Disposable {

    protected time_elapsed: number = 0

    protected spawn_max: number = 10
    protected spawn_delay: number = 1
    protected spawn_radius: number = 50

    public readonly disposables = new Disposable.Collection()
    public readonly zombies = new List<Entity>()

    constructor(
        protected entities: EntityManager,
    ) {
        this.disposables.push(LogicManager.Instance.register(this))
    }

    public update(delta_time: number) {
        if (this.zombies.length < this.spawn_max) {
            if (this.time_elapsed >= this.spawn_delay) {
                this.time_elapsed = 0
                this.spawn_zombie()
            } else {
                this.time_elapsed += delta_time
            }
        }
    }

    protected spawn_zombie() {
        const spawn_position = Vector3.Mult({
            x: Math.cos(Math.random() * Math.PI * 2),
            y: Math.sin(Math.random() * Math.PI * 2),
            z: 0,
        }, this.spawn_radius)

        const zombie = this.entities.spawn(GameEntities.Zombie)
        zombie.update_transform(spawn_position, Vector3.Null(), Vector3.Null())
        zombie.disposables.push(Disposable(() => this.zombies.remove(zombie)))
        this.zombies.push_head(zombie)

        console.log(`Spawn zombie! #${zombie.id}`)
    }

    public dispose(): void {
        this.disposables.dispose()
    }

}
