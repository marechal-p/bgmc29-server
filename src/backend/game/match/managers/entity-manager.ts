import { Disposable } from '../../../../common/disposable'

import { LogicManager } from '../../singletons/logic-manager'
import { EntityTemplate } from '../../entities/entity-template'
import { MatchEvent } from '../../match/match-events'
import { Entity } from '../../entities/entity'
import { Logic } from '../../logic'

export class EntityManager implements Logic, Disposable {

    protected readonly _entities = new Map<number, Entity>()

    public readonly disposables = new Disposable.Collection()

    constructor(
        protected readonly emit: MatchEvent.Emit,
    ) {
        this.disposables.push(LogicManager.Instance.register(this))
    }

    public [Symbol.iterator](): Iterator<Entity> {
        return this._entities.values()
    }

    public spawn<T extends any[]>(template: EntityTemplate<T>, ...args: T): Entity {
        const entity = new Entity(template.id, template.trait_factory(...args))
        this._entities.set(entity.id, entity)

        entity.disposables.push(() => this.delete(entity))

        this.emit(MatchEvent.EntityCreate.create(entity))
        return entity
    }

    public update(): void {
        this.emit(MatchEvent.EntityUpdate.create(Array.from(this)))
    }

    public get(id: number): Entity | undefined {
        return this._entities.get(id)
    }

    public delete(id: Entity | number): this {
        if (typeof id !== 'number') id = id.id // Entity.id
        const entity = this._entities.get(id)
        this._entities.delete(id)
        this.emit(MatchEvent.EntityDelete.create(entity))
        return this
    }

    public dispose(): void {
        if (!this.disposables.disposed) {
            for (const entity of this._entities.values()) {
                entity.dispose()
            }
        }
        this.disposables.dispose()
    }

}
