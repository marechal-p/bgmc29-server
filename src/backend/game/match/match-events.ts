import { Event } from '../../events'

import { Entity } from '../entities/entity'
import { Weapon } from '../items/weapon'
import { Vector3 } from '../vector'

export interface MatchEvent {

    'entity/create': {
        entity: Entity,
    }

    'entity/update': {
        entities: Entity[],
    }

    'entity/attack': {
        attacker: Entity,
        hit: Entity | undefined,
        weapon: Weapon,
        origin: Vector3,
        impact: Vector3,
    }

    'entity/delete': {
        entity: Entity,
    }

}
export namespace MatchEvent {
    export type Type<T extends keyof MatchEvent> = MatchEvent[T]
    export type Emit = (event: Event) => void
    export type Listen = (listener: (event: Event) => void) => void

    export const EntityCreate = new Event.Factory('entity/create',
        (entity: Entity) => ({ entity }) as MatchEvent['entity/create'])

    export const EntityUpdate = new Event.Factory('entity/update',
        (entities: Entity[]) => ({ entities } as MatchEvent['entity/update']))

    export const EntityAttack = new Event.Factory('entity/attack',
        (event: MatchEvent['entity/attack']) => event)

    export const EntityDelete = new Event.Factory('entity/delete',
        (entity: Entity) => ({ entity }) as MatchEvent['entity/delete'])
}
