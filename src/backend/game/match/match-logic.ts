import { EntityManager } from '../match/managers/entity-manager'
import { LivingEntity } from '../entities/traits/living'
import { Entity } from '../entities/entity'
import { GameEntities } from '../entities'
import { Message } from '../protocol'
import { Vector3 } from '../vector'
import { Client } from '../client'

import { ZombieManager } from './managers/zombie-manager'
import { MatchEvent } from './match-events'
import { MatchServer } from './match-server'

export class Match {

    /**
     * Entities tracked for this match
     */
    public readonly entities = new EntityManager(this.emit)

    /**
     * Zombies
     */
    public readonly zombies = new ZombieManager(this.entities)

    /**
     * Mapping `Client` -> `Entity.id`
     */
    public readonly players = new Map<Client, number>()

    constructor(
        match_server: MatchServer,
        protected emit: MatchEvent.Emit = (event: Event) => match_server.queue(event),
    ) { }

    protected get_player(client: Client): Entity | undefined {
        return this.entities.get(this.players.get(client))
    }

    public player_connect(client: Client): void {
        const player = this.entities.spawn(GameEntities.Player, client.name)
        this.players.set(client, player.id)

        const spawn_position = Vector3.Mult(Vector3.Random({ x: true, y: true }), 5)
        spawn_position.z += 2 // avoid ground glitch
        Vector3.Update(player.position, spawn_position)

        client.send(Message.Encode.Identification({
            id: player.id,
            position: player.position,
        }))

        for (const entity of this.entities) {
            client.send(Message.Encode.EntityCreate({
                entity_type: entity.type,
                entity_id: entity.id,
            }))
        }
    }

    public player_update(client: Client, message: Buffer): void {
        const player = this.get_player(client)
        const { position, rotation, velocity } = Message.Decode.PlayerUpdateMessage(message)
        player.update_transform_validate(position, rotation, velocity)
    }

    public player_attack(client: Client, message: Buffer): void {
        const player = this.get_player(client)
        const fired = Message.Decode.PlayerFireMessage(message)

        const test_weapon_id = Message.Null.UInt32
        const test_weapon_damage = 10

        const hit = this.entities.get(fired.hit)
        // const weapon = ItemManager.Instance.get(fired.weapon)
        if (fired.weapon !== test_weapon_id /* && !weapon */) {
            return console.warn(`Invalid weapon #${fired.weapon}`)
        }

        if (hit) {
            hit.handle(LivingEntity.DamageEvent.create('player', test_weapon_damage))
        }

        this.emit(MatchEvent.EntityAttack.create({
            attacker: player, hit,
            weapon: { id: test_weapon_id } as any, // TODO: real weapon
            origin: fired.origin,
            impact: fired.impact,
        }))
    }

    public player_disconnect(client: Client): void {
        this.get_player(client).dispose()
    }

    public player_die(): void { }
    public enemy_spawn(): void { }
    public enemy_die(): void { }

}
