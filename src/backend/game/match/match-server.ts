import * as WebSocket from 'ws'
import * as http from 'http'
import { isBuffer } from 'util'

import { PlayerTable } from '../../../database/player-table'
import { Database } from '../../../database'

import { GameServer } from '../../game-server'
import { Remote } from '../../remote'
import { Player } from '../../player'

import { Entity } from '../entities/entity'
import { Message } from '../protocol'
import { Client } from '../client'
import { Queue } from '../queue'

import { WebClientHandler } from './message-handlers/webclient-handler'
import { PlayerHandler } from './message-handlers/player-handler'
import { MatchEvent } from './match-events'
import { Match } from './match-logic'

export const MatchFactory = (
    match_server: MatchServer,
    emit?: MatchEvent.Emit,
) =>
    new Match(match_server, emit)

export class MatchServer {

    protected static readonly MAX_PLAYERS = 16
    protected static readonly MAX_REGISTRATION_PER_HOUR = 5

    protected readonly webclient_server_handler: WebClientHandler
    protected readonly player_server_handler: PlayerHandler
    protected readonly event_queue = new Queue<Event>()

    protected readonly match: Match

    constructor(
        game_server: GameServer,
        public readonly id: number,
        match = MatchFactory,
    ) {
        this.match = match(this)
        this.webclient_server_handler = new WebClientHandler(game_server.create_websocket_server(`game/info/${id}`))
        this.player_server_handler = new PlayerHandler(game_server.create_websocket_server(`game/play/${id}`, {
            verifyClient: this.authenticate_player.bind(this),
        }))
        this.webclient_server_handler.server.on('connection', this.webclient_connection.bind(this))
        this.player_server_handler.server.on('connection', this.player_connection.bind(this))
    }

    public queue(event: Event): void {
        this.event_queue.queue(event)
    }

    public get player_count(): number {
        return this.player_server_handler.server.clients.size
    }

    public update() {
        for (const event of this.event_queue.iter_pop_left()) {
            this.webclient_server_handler.handle(event)
            this.player_server_handler.handle(event)
        }
    }

    protected async webclient_connection(ws: WebSocket, request: http.IncomingMessage): Promise<void> {

    }

    protected async authenticate_player(info: WebSocket.VerifyClientInfo, callback: WebSocket.VerifyClientCallback): Promise<void> {
        const uuid = info.req.headers['bgmc-retro-uuid'] as string
        if (!uuid) {
            return callback(false, 403)
        }
        const { rows: [player], rowCount } = await Database.query(PlayerTable.GetPlayer(uuid))
        if (rowCount !== 1) {
            return callback(false, 403)
        }
        console.log(`Player authenticated: ${JSON.stringify(player)}`)
        ; (info.req as any)[Player.KEY] = new Player(player)
        return callback(true)
    }

    protected player_connection(ws: WebSocket, request: http.IncomingMessage): void {
        const client = new Client(Remote.FromRequest(request), ws, undefined)
        const player = (request as any)[Player.KEY] as Player

        client.websocket.on('message', message => this.handle_message(client, message))
        client.websocket.on('close', (code, reason) => this.disconnect(client))

        console.log(`Player connected! ${player.uuid}`)
        this.match.player_connect(client)
    }

    protected handle_message(client: Client, message: WebSocket.Data): void {
        if (!isBuffer(message)) {
            console.error(`${client.id}: message wasn't a buffer`)
            client.dispose()
            return
        }
        const [type, buffer] = Message.Decode.Message(message)
        switch (type) {
            case Message.Type.PlayerUpdate: return this.match.player_update(client, buffer)
            case Message.Type.PlayerAttack: return this.match.player_attack(client, buffer)
        }
    }

    protected disconnect(client: Client<Entity>): void {
        console.log(`Client disconnected: ${client.name}@${client.id}`)
        client.dispose()
    }

}
