import * as WebSocket from 'ws'

import { Disposable } from '../../common/disposable'

import { Remote } from '../remote'

export class Client<T = any> implements Disposable {

    public name: string = 'Unknown'
    public readonly disposables = new Disposable.Collection()

    constructor(
        public readonly remote: Remote,
        public readonly websocket: WebSocket,
        public handle: T,
    ) {
        this.disposables.push(() => this.websocket.close())
        if (Disposable.Is(handle)) {
            this.disposables.push(handle)
        }
    }

    get id(): string {
        return Remote.Id(this.remote)
    }

    public send(buffer: Buffer): void {
        if (this.websocket.readyState === WebSocket.OPEN) {
            this.websocket.send(buffer)
        }
    }

    public dispose() {
        this.disposables.dispose()
    }

}
