import { Atom } from '../atom'

export interface EntityTemplate<T extends any[]> {
    id: number
    trait_factory: (...args: T) => Atom.Trait[]
}
export function EntityTemplate<T extends any[]>(
    id: number,
    trait_factory: (...args: T) => Atom.Trait[],
) {
    if (EntityTemplate.Registered.has(id)) {
        throw new Error(`EntityTemplate.id already registered: ${id}`)
    }
    EntityTemplate.Registered.add(id)
    return Object.freeze({ id, trait_factory }) as EntityTemplate<T>
}
export namespace EntityTemplate {
    export const Registered = new Set<number>()
}
