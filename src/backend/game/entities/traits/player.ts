import { Entity } from '../entity'

export class PlayerTrait extends Entity.Trait {

    public kills = 0

    constructor(
        public readonly name: string,
    ) {
        super()
    }

}
