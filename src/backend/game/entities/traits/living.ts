import { Event } from '../../../events'

import { Entity } from '../entity'

export class LivingEntity extends Entity.Trait {

    public owner: Entity
    public health = this.max_health

    constructor(
        public readonly group = 'default',
    ) {
        super()
    }

    get max_health(): number {
        return 100
    }

    get alive(): boolean {
        return this.health > 0
    }

    get dead(): boolean {
        return !this.alive
    }

    handle(event: Event) {
        if (LivingEntity.DamageEvent.is(event) && event.group !== this.group) {
            this.damage(event.value)
            return true
        }
    }

    damage(value: number): void {
        this.health = Math.min(Math.max(this.health - value, 0), this.max_health)
        if (this.health === 0) {
            console.log(`Entity died #${this.owner.id}`)
            this.owner.dispose()
        }
    }

    heal(value: number): void {
        return this.damage(-value)
    }

}
export namespace LivingEntity {

    export interface DamageEvent {
        group: string
        value: number
    }
    export const DamageEvent = new Event.Factory('living-entity/damage',
        (group: string, value: number) => ({ group, value } as DamageEvent))

}
