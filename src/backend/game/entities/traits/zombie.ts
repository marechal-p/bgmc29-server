// import { EntityManager } from '../../singletons'
import { Vector3 } from '../../vector'

import { Entity } from '../entity'

// import { PlayerTrait } from './player'

export class ZombieAI extends Entity.Trait {

    protected trigger_distance = 15
    protected movement_speed = 5 // unit/second

    public owner: Entity

    public update(delta_time: number) {
        let target: Vector3 = { x: 0, y: 0, z: 3 }

        // const proximity: [number, Entity][] = []
        // for (const entity of []) {
        //     if (PlayerTrait.Has(entity)) {
        //         const distance = Vector3.Length(Vector3.Delta(entity.position, this.owner.position))
        //         if (distance < this.trigger_distance) {
        //             proximity.push([distance, entity])
        //         }
        //     }
        // }
        // proximity.sort(([a], [b]) => a - b)

        // if (proximity.length > 0) {
        //     target = proximity[0][1].position
        // }

        const velocity = Vector3.Mult(Vector3.Normalize(Vector3.Delta(target, this.owner.position)), this.movement_speed)
        const movement = Vector3.Mult(this.owner.velocity, delta_time)
        this.owner.set_position(Vector3.Add(movement, this.owner.position))
        this.owner.set_velocity(velocity)
    }

}
