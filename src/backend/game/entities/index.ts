import { EntityTemplate } from './entity-template'
import { LivingEntity } from './traits/living'
import { PlayerTrait } from './traits/player'
import { ZombieAI } from './traits/zombie'

export namespace GameEntities {

    export const Player = EntityTemplate(
        1, (player: any) => [
            new PlayerTrait(player),
            new LivingEntity('player'),
        ])

    export const Zombie = EntityTemplate(
        2, () => [
            new ZombieAI(),
            new LivingEntity('zombie'),
        ])

}
