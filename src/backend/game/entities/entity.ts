import { Vector3 } from '../vector'
import { Atom } from '../atom'

export class Entity extends Atom {

    protected static ID = 0

    public readonly id = Entity.ID++

    public readonly position: Vector3 = { x: 0, y: 0, z: 0 }
    public readonly rotation: Vector3 = { x: 0, y: 0, z: 0 }
    public readonly velocity: Vector3 = { x: 0, y: 0, z: 0 }

    constructor(
        public readonly type: number,
        traits: Atom.Trait[],
    ) {
        super(traits)
    }

    public update_transform(
        partial_position: Partial<Vector3> = { },
        partial_rotation: Partial<Vector3> = { },
        partial_velocity: Partial<Vector3> = { },
    ): void {
        Vector3.Update(this.position, partial_position)
        Vector3.Update(this.rotation, partial_rotation)
        Vector3.Update(this.velocity, partial_velocity)
    }

    public update_transform_validate(
        partial_position: Partial<Vector3> = { },
        partial_rotation: Partial<Vector3> = { },
        partial_velocity: Partial<Vector3> = { },
    ): void {
        const position = Vector3(partial_position)
        const rotation = Vector3(partial_rotation)
        const velocity = Vector3(partial_velocity)

        if (!this.validate(position, rotation, velocity)) {
            return
        }
        this.update_transform(position, rotation, velocity)
    }

    protected validate(
        position: Vector3,
        rotation: Vector3,
        velocity: Vector3,
    ): boolean {
        return true // TODO: Actually validate data
        const delta = Vector3.Delta(this.position, position)
        const delta_length = Vector3.Length(delta)
        if (delta_length > 5) {
            console.error(`invalid delta ${delta_length} ${JSON.stringify(delta)}`)
            return false
        }
        return true
    }

    public set_position(vector: Vector3) {
        this.position.x = vector.x
        this.position.y = vector.y
        this.position.z = vector.z
    }

    public set_velocity(vector: Vector3) {
        this.velocity.x = vector.x
        this.velocity.y = vector.y
        this.velocity.z = vector.z
    }

}
