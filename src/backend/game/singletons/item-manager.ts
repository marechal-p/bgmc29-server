import { Singleton } from '../../singleton'

import { Item } from '../items/item'

export class ItemManager extends Singleton {

    public static readonly Instance = new ItemManager()

    protected readonly registered = new Map<number, Item>()

    public register(items: Item.Type[]): void {
        // TODO: implement
    }

    public get(id: number): Item | undefined {
        return this.registered.get(id)
    }

}
