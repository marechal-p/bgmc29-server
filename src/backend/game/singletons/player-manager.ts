import { Singleton } from '../../singleton'
import { Entity } from '../entities/entity'

export class PlayerManager extends Singleton {

    public static readonly Instance = new PlayerManager()

    public readonly _players = new Map<string, Entity>()

    public get players(): Entity[] {
        return [...this._players.values()]
    }

}
