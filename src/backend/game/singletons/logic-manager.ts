import { Disposable } from '../../../common/disposable'

import { Singleton } from '../../singleton'

import { Logic } from '../logic'

export interface LogicHandle<T extends Logic> extends Disposable {
    readonly id: number,
    readonly logic: T,
}
export class LogicManager extends Singleton {

    public static readonly Instance = new LogicManager()

    protected readonly _logics: Array<Logic | undefined> = []
    protected readonly _empty_slots: number[] = [] // queue

    public register<T extends Logic>(logic: T): LogicHandle<T> {
        let id = this._empty_slots.pop()

        if (typeof id === 'undefined') {
            id = this._logics.push(logic) - 1
        } else {
            this._logics[id] = logic
        }

        const manager = this
        return {
            id, logic,
            dispose() {
                manager.unregister(this.id)
            },
        }
    }

    public get<T extends Logic>(id: number): T {
        const logic = this._logics[id]
        if (typeof logic === 'undefined') {
            throw new Error(`undefined logic #${id}`)
        }
        return logic as T
    }

    public unregister(id: number): this {
        delete this._logics[id]
        this._empty_slots.push(id)
        return this
    }

    public update(delta_time: number): void {
        for (const logic of this._logics) {
            if (!logic) continue
            try {
                logic.update(delta_time)
            } catch (error) {
                console.error(error)
            }
        }
    }

}
