import { Entity } from './entities/entity'
import { Weapon } from './items/weapon'
import { Vector3 } from './vector'
import { Struct } from './struct'

export namespace Message {

    export enum Type {
        PlayerUpdate = 0,
        PlayerAttack = 1,
        PlayerDeath = 2,
        EntityDelete = 3,
        EntityUpdate = 4,
        EntityAttack = 5,
        EntityDeath = 6,
        EntityCreate = 7,
        Identification = 8,
    }

    /**
     * Max value represents "Null".
     * 0 is too common, while an exact max value is less common.
     */
    export namespace Null {
        export const UInt32 = 0xFFFFFFFF
        export const UInt8 = 0xFF
    }

    export namespace Format {

        /**
         * Generic id field: int32
         */
        export const Id = new Struct<[number]>('<I')

        export const MessageHeader = new Struct<[Message.Type]>('<B')

        export const IdentificationMessage = new Struct.Concat(
            Id, Vector3.Struct)

        export const EntityCreate = new Struct<[number, number]>('<IB')

        export const EntityUpdateHead = new Struct<[number]>('<I')
        export const EntityUpdate = new Struct.Concat(
            Id, Vector3.Struct, Vector3.Struct, Vector3.Struct)

        export const EntityDelete = new Struct<[number]>('<I')

        export const PlayerUpdate = new Struct.Concat(
            Vector3.Struct, Vector3.Struct, Vector3.Struct)

        export const PlayerAttack = new Struct.Concat(
            Id, Id, Vector3.Struct, Vector3.Struct)

        export const EntityAttack = new Struct.Concat(
            Id, Id, Id, Vector3.Struct, Vector3.Struct)

    }

    export namespace Encode {

        /**
         * Base message to send to client
         */
        export function Message(type: Type, message: Buffer): Buffer {
            return Buffer.concat([Format.MessageHeader.pack(type), message])
        }

        /**
         * Notification message for the client to identify itself
         */
        export interface Identification {
            id: number,
            position: Vector3
        }
        export function Identification(message: Identification): Buffer {
            return Message(Type.Identification, Format.IdentificationMessage.pack(
                [message.id], [message.position.x, message.position.y, message.position.z]))
        }

        /**
         * Notification message when an entity is created
         */
        export interface EntityCreate {
            entity_id: number
            entity_type: number
        }
        export function EntityCreate(message: EntityCreate): Buffer {
            return Message(Type.EntityCreate, Format.EntityCreate.pack(
                message.entity_id, message.entity_type))
        }

        /**
         * Message send with the new parameters for each entity
         */
        export function EntityUpdate(entities: Entity[]): Buffer {
            return Message(Type.EntityUpdate, Buffer.concat([
                Format.EntityUpdateHead.pack(entities.length),
                ...entities.map(entity => Format.EntityUpdate.pack(
                    [entity.id],
                    Vector3.ToArray(entity.position),
                    Vector3.ToArray(entity.rotation),
                    Vector3.ToArray(entity.velocity),
                )),
            ]))
        }

        /**
         * Notification message sent when an entity is deleted
         */
        export function EntityDelete(entity: Entity): Buffer {
            return Message(Type.EntityDelete, Format.EntityDelete.pack(entity.id))
        }

        export interface EntityAttack {
            attacker: Entity
            hit?: Entity
            weapon: Weapon
            origin: Vector3
            impact: Vector3
        }
        export function EntityAttack(message: EntityAttack): Buffer {
            return Message(Type.EntityAttack, Format.EntityAttack.pack(
                [message.attacker.id],
                [message.hit ? message.hit.id : Null.UInt32],
                [message.weapon.id],
                Vector3.ToArray(message.origin),
                Vector3.ToArray(message.impact),
            ))
        }

    }

    export namespace Decode {

        /**
         * Decapitates a received message and returns the head followed by the body
         *
         * @param raw whole message with header
         */
        export function Message(message: Buffer): [Type, Buffer] {
            const [[type], body] = Format.MessageHeader.unpack_from_head(message)
            return [type, body]
        }

        /**
         * Notification message received from client with its new parameters
         */
        export interface PlayerUpdate {
            position: Vector3
            rotation: Vector3
            velocity: Vector3
        }
        export function PlayerUpdateMessage(buffer: Buffer): PlayerUpdate {
            const [position, rotation, velocity] = Format.PlayerUpdate.unpack(buffer)
            return {
                position: Vector3.FromArray(position),
                rotation: Vector3.FromArray(rotation),
                velocity: Vector3.FromArray(velocity),
            }
        }

        export interface PlayerFire {
            hit: number
            weapon: number
            origin: Vector3
            impact: Vector3
        }
        export function PlayerFireMessage(buffer: Buffer): PlayerFire {
            const [[hit], [weapon], origin, impact] = Format.PlayerAttack.unpack(buffer)
            return {
                hit,
                weapon,
                origin: Vector3.FromArray(origin),
                impact: Vector3.FromArray(impact),
            }
        }

    }

}
