import { Struct as DataStruct } from './struct'

export interface Vector3 {
    x: Vector3.X
    y: Vector3.Y
    z: Vector3.Z
}
export function Vector3(vector: Partial<Vector3>): Vector3 {
    return Vector3.Update(Vector3.Null(), vector)
}
export namespace Vector3 {

    export type X = number
    export type Y = number
    export type Z = number

    export const Struct = new DataStruct<[X, Y, Z]>('<3f')

    export function ToArray(vector: Vector3): [X, Y, Z] {
        return [vector.x, vector.y, vector.z]
    }

    export function FromArray(vector: [X, Y, Z]): Vector3 {
        return { x: vector[0], y: vector[1], z: vector[2] }
    }

    /**
     * Default null vector.
     */
    export function Null(): Vector3 {
        return { x: 0, y: 0, z: 0 }
    }

    /**
     * Fill every fields of a vector.
     *
     * @param vector Vector to fill.
     * @param value Value to use.
     */
    export function Fill(vector: Partial<Vector3>, value: number = 0): Vector3 {
        if (typeof vector.x !== 'number') vector.x = value
        if (typeof vector.y !== 'number') vector.y = value
        if (typeof vector.z !== 'number') vector.z = value
        return vector as Vector3
    }

    /**
     * Fill every fields of a copy of a vector.
     *
     * @param vector Vector to fill a copy.
     * @param value Value to use.
     */
    export function Filled(source: Partial<Vector3>, value: number = 0): Vector3 {
        return Vector3.Fill(Vector3(source), value)
    }

    /**
     * Update the values of a vector.
     *
     * @param target Vector to update.
     * @param source New values to apply.
     */
    export function Update(target: Vector3, source: Partial<Vector3>): Vector3 {
        if (typeof source.x === 'number') target.x = source.x
        if (typeof source.y === 'number') target.y = source.y
        if (typeof source.z === 'number') target.z = source.z
        return target
    }

    /**
     * Update the values of a vector and returns a copy.
     *
     * @param target Vector to copy and update.
     * @param source New values to apply.
     */
    export function Updated(target: Vector3, source: Partial<Vector3>): Vector3 {
        return Vector3.Update(Vector3(target), source)
    }

    export function Delta(a: Vector3, b: Vector3): Vector3 {
        return {
            x: a.x - b.x,
            y: a.y - b.y,
            z: a.z - b.z,
        }
    }

    export function Add(a: Vector3, b: Vector3): Vector3 {
        return {
            x: a.x + b.x,
            y: a.y + b.y,
            z: a.z + b.z,
        }
    }

    export function Mult(vector: Vector3, factor: number): Vector3 {
        return {
            x: vector.x * factor,
            y: vector.y * factor,
            z: vector.z * factor,
        }
    }

    export function Length(vector: Vector3): number {
        return Math.sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
    }

    export function Normalize(vector: Vector3): Vector3 {
        const length = Vector3.Length(vector)
        if (length === 0) {
            return vector
        }
        vector.x /= length
        vector.y /= length
        vector.z /= length
        return vector
    }

    export function Normalized(vector: Vector3): Vector3 {
        const length = Vector3.Length(vector)
        if (length === 0) {
            return Vector3(vector)
        }
        return {
            x: vector.x / length,
            y: vector.y / length,
            z: vector.z / length,
        }
    }

    export interface RandomOptions {
        x?: boolean
        y?: boolean
        z?: boolean
    }
    export function Random(options: RandomOptions = { }): Vector3 {
        const base = Vector3.Null()
        if (options.x) base.x = Math.random() * 2 - 1
        if (options.y) base.y = Math.random() * 2 - 1
        if (options.z) base.z = Math.random() * 2 - 1
        return Vector3.Normalized(base)
    }

}
