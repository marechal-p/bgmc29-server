import struct from 'python-struct'

export class Struct<T extends struct.value[] = struct.value[]> {

    public readonly size = struct.sizeOf(this.format)

    constructor(
        public readonly format: string,
    ) { }

    public pack(...values: T): Buffer {
        return struct.pack<T>(this.format, ...values)
    }

    public pack_into(buffer: Buffer, offset: number = 0, ...values: T): void {
        const packed = this.pack(...values)
        for (let i = this.size; i >= 0; i--) {
            buffer[offset + i] = packed[i]
        }
    }

    public unpack(buffer: Buffer): T {
        return struct.unpack<T>(this.format, buffer)
    }

    public unpack_from(buffer: Buffer, offset: number = 0): T {
        return this.unpack(buffer.slice(offset, offset + this.size))
    }

    public unpack_from_head(buffer: Buffer): [T, Buffer] {
        return [this.unpack_from(buffer), buffer.slice(this.size)]
    }

    public *iter_unpack(buffer: Buffer): Iterator<T> {
        const modulo = buffer.length % this.size
        if (modulo !== 0) {
            throw new Error(`${buffer.length} % ${this.size} === ${modulo} !== 0`)
        }
        for (let i = 0; i + this.size < buffer.length; i += this.size) {
            yield this.unpack(buffer.slice(i, this.size))
        }
    }

}
export namespace Struct {

    export type Value = struct.value

    type ExtractValueFromStruct<T> = T extends Struct<infer V> ? V : never
    type MapStructTuple<T extends Struct[]> = {
        [P in keyof T]: ExtractValueFromStruct<T[P]>
    }

    /**
     * A group of struct that form a bigger structure together
     */
    export class Concat<T extends Struct[] = Struct[]> {

        protected readonly structs = [] as T

        constructor(...structs: T) {
            this.structs = structs
        }

        public pack(...args_list: MapStructTuple<T>): Buffer {
            return Buffer.concat(args_list.map((args, index) => this.structs[index].pack(...args)))
        }

        public unpack(buffer: Buffer): MapStructTuple<T> {
            let offset = 0
            return this.structs.map(struct => {
                const data = struct.unpack_from(buffer, offset)
                offset += struct.size
                return data
            }) as any
        }

    }

}
