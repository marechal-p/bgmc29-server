export interface Logic {
    update(delta_time: number): void
}
