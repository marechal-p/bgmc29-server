import { Disposable } from '../../common/disposable'

import { Event } from '../events'

import { LogicManager } from './singletons/logic-manager'
import { Logic } from './logic'

export class Atom implements Event.Handler, Disposable {

    protected readonly _traits: number[] = []

    public readonly disposables = new Disposable.Collection()

    constructor(
        traits: Atom.Trait[],
    ) {
        for (const trait of traits) {
            const handle = LogicManager.Instance.register(trait)
            this.disposables.push(handle, trait)
            this._traits.push(handle.id)
            trait.initialize(this)
        }
    }

    public get traits(): Atom.Trait[] {
        return this._traits.map(id => LogicManager.Instance.get(id))
    }

    public get_trait<T extends Atom.Trait>(type: { new(...args: any[]): T }): T | undefined {
        return this.get_traits(type)[0]
    }

    public get_traits<T extends Atom.Trait>(type: { new(...args: any[]): T }): T[] {
        function filter(trait: Atom.Trait): trait is T {
            return trait.constructor === type
        }
        return this.traits.filter<T>(filter)
    }

    public handle(event: Event) {
        for (const trait of this.traits) {
            if (trait.handle(event)) return true
        }
    }

    public dispose() {
        this.disposables.dispose()
    }

}
export namespace Atom {

    export abstract class Trait implements Logic, Event.Handler, Disposable {

        public static Has(atom: Atom): boolean {
            return atom.traits.some(trait => trait instanceof this)
        }

        public static Get(atom: Atom): Trait[] {
            return atom.traits.filter(trait => trait instanceof this)
        }

        public owner: Atom | undefined

        public update(delta_time: number): void { }
        public handle(event: Event): boolean | void { }
        public dispose(): void { }

        public initialize(owner: Atom): void {
            this.owner = owner
        }

    }

}
