import { EventEmitter } from 'events'

type CastString<T> = T extends string ? T : never
type CastArray<T> = T extends any[] ? T : T extends object ? [T] : never

export interface TypedEventEmitter<T extends object> {
    addListener<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    on<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    once<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    prependListener<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    prependOnceListener<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    removeListener<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    off<K extends keyof T>(event: CastString<K>, listener: (...args: CastArray<T[K]>) => void): this
    removeAllListeners<K extends keyof T>(event?: CastString<K>): this
    setMaxListeners(n: number): this
    getMaxListeners(): number
    listeners<K extends keyof T>(event: CastString<K>): Function[]
    rawListeners<K extends keyof T>(event: CastString<K>): Function[]
    emit<K extends keyof T>(event: CastString<K>, ...args: CastArray<T[K]>): boolean
    eventNames(): Array<string | symbol>
    listenerCount<K extends keyof T>(type: CastString<K>): number
}
export class TypedEventEmitter<T extends object> extends EventEmitter {
    public static listenerCount<E extends object>(emitter: TypedEventEmitter<E>, event: CastString<keyof E>): number {
        return super.listenerCount(emitter as EventEmitter, event as any)
    }
}
