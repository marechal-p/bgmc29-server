import { Disposable } from '../common/disposable'
import { MaybePromise } from '../common/types'

export class Emitter<T extends object = object> {

    protected _listeners: [Emitter.Listener<T>, any][] = []

    public register = this._register.bind(this)

    protected _register(listener: Emitter.Listener<T>, context: any = undefined): Disposable {
        const item: [Emitter.Listener<T>, any] = [listener, context]
        this._listeners.push(item)
        return Disposable(() => this._unregister(item))
    }

    protected _unregister(item: any) {
        const index = this._listeners.indexOf(item)
        if (index !== -1) {
            this._listeners.splice(index, 1)
        }
    }

    public async sequence(event: T): Promise<void> {
        for (const [listener, context] of this._listeners) {
            await listener.apply(context, [event])
        }
    }

    public async parallel(event: T): Promise<void> {
        await Promise.all(this._listeners.map(
            async ([listener, context]) => listener.apply(context, [event]),
        ))
    }

}
export namespace Emitter {

    export type Listener<T extends object = object> = (event: T) => MaybePromise<void>

}
