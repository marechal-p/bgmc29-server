import { PlayerState } from '../common/player-data'
import * as React from 'react'

export interface PlayerProps {
    name: string
    kills?: number
    health?: number
}
export class Player extends React.Component<PlayerProps, PlayerState> {

    constructor(props: PlayerProps) {
        super(props)
        this.state = {
            name: props.name,
            health: props.health || 0,
            kills: props.kills || 0,
        }
    }

    public render() {
        return <div className='player'>
            <span className='player-name'>{ this.state.name }</span>: {
            }<span className='player-kills'>{ this.state.kills }</span> kills {
            }<span className='player-health'>{ this.state.health }</span> health
        </div>
    }
}
