import * as ReactDOM from 'react-dom'
import * as React from 'react'

import { GameWebMessage } from '../common/game-events'
import { PlayerState } from '../common/player-data'

import { Player } from './player'

const server_address = new URL('/ws/game/info/0', window.location.href)
server_address.protocol = server_address.protocol.replace('http', 'ws')

interface MainState {
    players: PlayerState[]
}
class Main extends React.Component<any, MainState> {

    public readonly connection = new WebSocket(server_address.href)

    constructor(props: any) {
        super(props)
        this.state = { players: [] }
        this.connection.addEventListener('message', this.message.bind(this))
        this.connection.addEventListener('close', this.close.bind(this))
    }

    protected message(event: MessageEvent): void {
        const message: GameWebMessage = JSON.parse(event.data)
        if (message.type === 'player-list-update') {
            this.setState({ players: message.body })
        }
    }

    protected close(event: CloseEvent): void {
        console.debug('WebSocket was closed :(')
    }

    public render() {
        const players = this.state.players
        return <React.Fragment>
            <header>
                <h1>Zombie Game</h1>
            </header>
            <nav>
                <h3>Players:</h3>
                <div id='player-list'>{ players.length ?
                    // Right now I am not updating the state of the living players
                    // TODO: Stop recreating new react widget and update in place.
                    players.map(d => <Player name={ d.name } health={ d.health } />) :
                    <div>No one is playing...</div>
                }</div>
            </nav>
            <footer>
                <a href='https://github.com/marechal-p'>wkk.py</a>
            </footer>
        </React.Fragment>
    }

}

ReactDOM.render(<Main />, document.getElementById('application'))
