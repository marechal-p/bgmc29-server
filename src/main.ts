import { GameServer } from './backend/game-server'
import { Database } from './database'

async function main(): Promise<GameServer> {
    await Database.Initialize()

    const port = Number.parseInt(process.env['PORT'], 10) || 1234
    const host = process.env['HOST'] || '0.0.0.0'

    return new GameServer(host, port)
}

export = main()
