import { QueryConfig } from 'pg'

export const Extensions: { [key in string]: QueryConfig } = {
    'UUID-OSSP': {
        text: 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";',
    },
}
