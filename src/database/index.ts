import { Pool } from 'pg'

// import { Extensions } from './extensions'
import { PlayerTable } from './player-table'
import { IpTable } from './ip-table'

export namespace Database {

    export const ConnectionPool = new Pool({
        connectionString: process.env['DATABASE_URL'],
        max: 10,
    })

    export const query: Pool['query'] = ConnectionPool.query.bind(ConnectionPool)
    export const connect: Pool['connect'] = ConnectionPool.connect.bind(ConnectionPool)

    export async function Initialize(): Promise<void> {
        const client = await Database.connect()
        try {

            // Load PostgreSQL extensions
            await Promise.all([
                // Extensions['UUID-OSSP'],
            ].map(async q => client.query(q)))

            // Create tables
            await Promise.all([
                IpTable.CreateIpTable,
                PlayerTable.CreatePlayerTable,
            ].map(async q => client.query(q)))

        } finally {
            client.release()
        }

    }

}
