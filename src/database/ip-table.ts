import { QueryConfig } from 'pg'

export namespace IpTable {

    export const CreateIpTable = `
        CREATE TABLE IF NOT EXISTS ips (
            address VARCHAR(16) NOT NULL,
            time TIMESTAMP NOT NULL DEFAULT NOW()
        )`

    export function InsertIpEntry(address: string): QueryConfig {
        return {
            text: 'INSERT INTO ips(address) VALUES ($1)',
            values: [address],
        }
    }

    export function GetIpEntry(address: string): QueryConfig {
        return {
            text: 'SELECT address, time FROM ips WHERE address = $1',
            values: [address],
        }
    }

    export function GetMostRecentIpEntries(address: string, limit: number): QueryConfig {
        return {
            text: `
                SELECT count(*) FROM (
                    SELECT address, time FROM ips
                    WHERE address = $1
                    AND time >= NOW() - interval '1 hour'
                    LIMIT $2
                ) AS entries
            `,
            values: [address, limit],
        }
    }

}
