import { QueryConfig } from 'pg'

export namespace PlayerTable {

    export namespace Constraints {
        export const UniquePlayerUsername = 'players_username_unique'
    }

    export const CreatePlayerTable = `
        CREATE TABLE IF NOT EXISTS players (
            uuid UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
            username VARCHAR(127) NOT NULL,
            kills INTEGER DEFAULT 0,

            CONSTRAINT ${Constraints.UniquePlayerUsername} UNIQUE (username)
        )`

    export function InsertPlayer(uuid: string, username: string): QueryConfig {
        return {
            text: 'INSERT INTO players(uuid, username) VALUES ($1, $2)',
            values: [uuid, username],
        }
    }

    export function GetPlayer(uuid: string): QueryConfig {
        return {
            text: 'SELECT uuid, username, kills FROM players WHERE uuid = $1',
            values: [uuid],
        }
    }

    export function UpdatePlayerUsername(uuid: string, username: string): QueryConfig {
        return {
            text: 'UPDATE players SET username = $2 WHERE uuid = $1',
            values: [uuid, username],
        }
    }

    export function UpdatePlayerKills(uuid: string, add: number = 1): QueryConfig {
        return {
            text: 'UPDATE players SET kills = kills + $2 WHERE uuid = $1',
            values: [uuid, add],
        }
    }

}
