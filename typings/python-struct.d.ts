declare module 'python-struct' {

    export = struct

    namespace struct {

        export type value = boolean | number | string | null | undefined // or any?

        export function sizeOf(format: string): number

        export function pack<T extends struct.value[] = struct.value[]>(format: string, ...args: T): Buffer

        export function unpack<T extends struct.value[] = struct.value[]>(format: string, buffer: Buffer): T

    }

}
