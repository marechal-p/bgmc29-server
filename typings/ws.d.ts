import * as http from 'http'

declare module 'ws' {

    interface VerifyClientInfo {

        /**
         * The value in the Origin header indicated by the client.
         */
        origin: string

        /**
         * The client HTTP GET request.
         */
        req: http.IncomingMessage

        /**
         * `true` if `req.connection.authorized` or `req.connection.encrypted` is set.
         */
        secure: boolean
    }

    type VerifyClientCallback = (

        /**
         * Whether or not to accept the handshake.
         */
        result: boolean,

        /**
         * When `result` is `false` this field determines the HTTP error status code to be sent to the client.
         */
        code?: number,

        /**
         * When `result` is `false` this field determines the HTTP reason phrase.
         */
        name?: string,

        /**
         * When `result` is `false` this field determines additional HTTP headers to be sent to the client. For example, `{ 'Retry-After': 120 }`.
         */
        headers?: object,

    ) => void

}
